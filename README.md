# gm_rename_motd Cookbook

-   This cookbook removes the /etc/motd and /etc/issue files.
-   These files are removed so they don't interfere with the output of pdsh commands.

## Requirements

- Valid chef environment

### Platforms

- OEL 6.9 (Probably is not specific that just that, but only tested here)

### Chef

- Chef 12.0 or later

### Cookbooks

- No dependencies on other cookbooks


## Usage

This cookbook is comprised of 1 recipe:   

   *  default.rb    
      This recipe removes the specified files
   
   
     
## License and Authors

Authors: Rick Connelly & James Mullaney

