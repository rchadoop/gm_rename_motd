title 'rename motd files'
control 'etc_motd-1.0' do

 impact 0.3
  title 'verify no motd files'
  desc '/etc/motd and /etc/issue should be removed'
  describe file('/etc/motd') do
    it { should_not exist}
  end
  describe file('/etc/issue') do
    it { should_not exist}
  end
end

